function mask = maskImage(colorImage)

% Scale is now [0,1] for HSV not HSL. VERY DIFFERENT.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% HSV COLORS
% Jon - 39/360, .31, .98; 30/360, 27,97
% Garfield - 34/360, .79, .93; 35/360, .85, .91
% Odie - 56/360, 44,100; BAD - 60, .34 .98
% Jon's hair - 15/360, .66, .6; BAD: 43/360, .34, .71
% Nermal - 60/360, .07, .64
% Garfield Stripes - 30, 100, 35

% FORMAT IS: color image, min hue, max hue, min sat, max sat, min val, max
% val
    orangeMask = colorMask(colorImage,29/360,37/360,.6,1,.7,1);
    garfieldOldMask = colorMask(colorImage,35/360,40/360, .85,1,.95,1);
    yellowMask = colorMask(colorImage,50/360,60/360,.35,.55,.9,1);
    tanMask = colorMask(colorImage, 25/360,50/360,.2,.4,.9,1);
    grayMask = colorMask(colorImage, 40/360, 100/360, 0, .20,.25,.75);
    brownMask = colorMask(colorImage,15/360,25/360, .65, .9, .4, .7);
    blackMask = colorMask(colorImage,0,1,0,1,0,.1);
    stripeMask = colorMask(colorImage,25/360,35/360, .9,1,.2,.4);
%     oldStripeMask = colorMask(colorImage,35/360, DO THIS
%     whiteMask = colorMask(colorImage,0,1,0,.1,.95,1);
%     redMask = colorMask(colorImage,330/360,1,0,1,0,1) | colorMask(colorImage,0,30/360,0,1,0,1);
    mask = orangeMask | yellowMask  | tanMask | grayMask | brownMask | stripeMask | garfieldOldMask;
%     mask = orangeMask;
end








%%%%%%%%%%%%%%%%%%%%%%%%%%%% IGNORE OLD STUFF
% SUPER CONFUSING SCALE!
% H - 0:360
% S - 0:100
% V - 0:100

% HOW HSV IS SCALED IS VERY IMPORTANT HERE!

% Odie is a (37,240,179) on (240,240,240) scale
% Jon is a 26, 213, 190; 26,178,189; 26, 194, 196
% Nermal is 34, 231, 130

% 29, 131, 172 is ground
%% OLD VALS
%     orangeMask = colorMask(colorImage,2,55,10,90,10,90);
%     yellowMask = colorMask(colorImage,55,65,90,100,50,100);
%   
%% 2013 Pic Set
%     orangeMask = colorMask(colorImage,30,35,50,100,50,100);
%     yellowMask = colorMask(colorImage,50,65,35,60,75,100);
%     tanMask = colorMask(colorImage, 35,45,30,60,50,100);
%     grayMask = colorMask(colorImage, 40, 75, 0, 20,25,75) | colorMask(colorImage, 40, 75, 90, 100,25,75);
%     mask = orangeMask | yellowMask | tanMask | grayMask;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5