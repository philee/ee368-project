function hsvHistMat = blobToHist(blobs,mask, colorImage, showImages, meanTrainRow)
    
    if nargin < 5
        meanTrainRow = 0;
    end

    vertOffset = 10;
    horzOffset = 5;
    hsvHistMat = zeros(length(blobs),256);
    [rows cols] = size(mask);

    for i=1:length(blobs)
        curBBox = blobs(i).BoundingBox;
        x = max(floor(curBBox(1)-horzOffset),1);
        y = max(floor(curBBox(2)-vertOffset),1);
        maxX = min(cols,x+curBBox(3) + 2*horzOffset);
        maxY = min(rows,y+curBBox(4) + vertOffset);

        curImage = colorImage(y:maxY,x:maxX,:);
        curHSV = rgbToHSV(curImage);
        hist = imhist(curHSV(:,:,1))';
        normalizedHist = (hist - meanTrainRow);
        normalizedHist = normalizedHist / sum(normalizedHist);
        hsvHistMat(i,:) = normalizedHist;
        
        if showImages
            figure(); imshow(curImage);
        end
    end
end