function pass = CropImages(year)
    GARIDX = 1;
    JONIDX = 2;
    ODIEIDX = 3;
    LIZIDX = 4;
    ARLENEIDX = 5; 
    POOKYIDX = 6;
    NERMALIDX = 7;
    MISCIDX = 8;
    characters = {'Garfield','Jon','Odie','Liz','Arlene','Pooky','Nermal', 'Misc'};
    
    % Check for valid year
    isValid = checkValidYear(year);
    if ~isValid
        display ('Invalid Year')
        return
    end
    
    % Move to directory of year
    cd('Comics')
    if ischar(year)
        cd(year)
        numYear = str2num(year);
    else
        cd(num2str(year))
        numYear = year;
    end
    
    % Create character directories if they don't exist
    charNumArr = createCharDirectories(characters);
    
    % Create Progress Text if it doesn't exist
    curFileIndex = createProgressText();
    
    % For each file in folder
    files = dir;
    prompt = 'Input the first letter of the name of the character then enter \n (G)arfield,(J)on,(O)die, (L)iz, (A)rlene, (P)ooky, (N)ermal, (M)isc.\n (D)one to go to next picture\n (R)emove to remove previously croppedImages \n (Q)uit to stop cropping\n';

    % Iterate over files
    for k=curFileIndex:(length(files)-1)
        
        if length(files(k).name) < 4
            continue
        end
        
        fileType = files(k).name((end-3):end);
		if strcmp(fileType,'.png')
			[curImage, map] = imread(files(k).name,'png');
        elseif strcmp(fileType,'.gif')
			curImage = imread(files(k).name);
        else
            continue
		end
        [nRows nCols] = size(curImage);

        % Until you hit quit
        while strcmp(fileType,'.png')
            figure();
            imshow(curImage,map);
            charName = lower(input(prompt,'s'));
            
            if length(charName) < 1
                firstChar = 'q';
            else
                firstChar = charName(1);
            end
            
            if strcmp(firstChar,'d')
                break
            elseif strcmp(firstChar,'r')
                disp ('Deleting Previous file');
                prevFileName = fileName{1};
                disp(prevFileName);
                delete(prevFileName);
                charNumArr(curIdx) = charNumArr(curIdx) - 1;
                close
                continue
            elseif strcmp(firstChar,'q')
                close
                cd ../../
                return
            elseif ~isValidChar(firstChar)
                disp('Invalid Character Input \n');
                continue
            end
                
            disp('Click the upper left corner of a character');
            [x1 y1] = ginput(1);
            disp('Click the lower right corner of a character');
            [x2 y2] = ginput(1);
            close;
        
            [row1, row2, col1, col2] = sanitizeIndices(x1, x2, y1, y2, nRows, nCols);
                  
            [charName, curIdx] = getCharInfo (firstChar);
            
            cropImage = curImage(row1:row2,col1:col2,:);
            curNum = charNumArr(curIdx);
            fileName = strcat({charName},{'/'},{num2str(numYear)},{'_'},{charName},'_',{num2str(curNum)},{fileType});
            charNumArr(curIdx) = charNumArr(curIdx) + 1;
            
            if strcmp(fileType,'.png')
                try
                    imwrite(cropImage,map,fileName{1},'png');
                catch err
                    imwrite(cropImage,fileName{1},'png');
                end
            end
            
        end
        close
        appendCurFileName(files(k).name);
    end
    
    cd ../../
end

function valid = checkValidYear(yearIn)
    if ischar(yearIn)
        numYear = str2double(yearIn);
    else
        numYear = int16(yearIn);
    end
    
    if (numYear > 2014) || (numYear < 1982)
        disp('Invalid Year Entered')
        valid = false;
    else
        valid = true;
    end
end

function charNumArr = createCharDirectories (chars)
    characters = chars;
    charNumArr = zeros(8,1);
    for curInd = 1:length(characters)
        curChar = characters{curInd};
        if ~exist(curChar,'dir')
            mkdir(curChar)
        else
            [nFiles nameLength] = size(ls(curChar));
            charNumArr(curInd) = nFiles - 2;
        end
    end
end

function curFileIndex = createProgressText()
    if ~exist('progress.txt','file')
        fid = fopen('progress.txt','w');
        fprintf(fid,'Created Directories');
        fclose(fid);
        curFileIndex = 3;
    
    % Find current file number
    else
        % Read last line 
        fid = fopen('progress.txt');
        while true
            line = fgets(fid);
            if ~ischar(line)
                break
            end
            prevLine = line;
        end
        lastFile = prevLine;
                
        if strcmp(lastFile,'Created Directories')
            curFileIndex = 3;
        else
            files = dir;
            curFileIndex = 0;
            for i=1:length(files)
                if strcmp(files(i).name,lastFile)
                    curFileIndex = i+1;
                    break
                end
            end
        end
    end
end

function success = appendCurFileName(currentFile)
    success = false;
    fid = fopen('progress.txt','a');
    fprintf(fid,strcat('\n',currentFile));
    fclose(fid);
    success = true;
end

function [charName, curIdx] = getCharInfo(letter)
    GARIDX = 1;
    JONIDX = 2;
    ODIEIDX = 3;
    LIZIDX = 4;
    ARLENEIDX = 5; 
    POOKYIDX = 6;
    NERMALIDX = 7;
    MISCIDX = 8;
    
    switch letter
        case 'q'
            charName = 'Quit';
            curIdx = -1;
        case 'd'
            charName = 'Done';
            curIdx = -1;
        case 'g'
            charName = 'Garfield';
            curIdx = GARIDX;
        case 'j'
            charName = 'Jon';
            curIdx = JONIDX;
        case 'o'
            charName = 'Odie';
            curIdx = ODIEIDX;
        case 'l'
            charName = 'Liz';
            curIdx = LIZIDX;
        case 'a'
            charName = 'Arlene';
            curIdx = ARLENEIDX;
        case 'p'
            charName = 'Pooky';
            curIdx = POOKYIDX;
        case 'n'
            charName = 'Nermal';
            curIdx = NERMALIDX;
        case 'm'
            charName = 'Misc';
            curIdx = MISCIDX;
    end
end

function [row1, row2, col1, col2] = sanitizeIndices(x1, x2, y1, y2, nRows, nCols)
        row1 = max(floor(y1),1);
        row2 = min(ceil(y2),nRows);
        col1 = max(floor(x1),1);
        col2 = min(ceil(x2),nCols);
end

function good = isValidChar(input)
    switch input
        case 'q'
            good = true;
        case 'd'
            good = true;
        case 'g'
            good = true;
        case 'j'
            good = true;
        case 'o'
           good = true;
        case 'l'
            good = true;
        case 'a'
            good = true;
        case 'p'
            good = true;
        case 'n'
            good = true;
        case 'm'
            good = true;
        otherwise 
            good = false;
    end
end