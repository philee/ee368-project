function [coords, characters] = analyzeFrame(test,trainingData, labels)
    %%%%%%%%%%%%%%%% AUTOMATIC COLOR SEGMENTATION%%%%%%%%%%%%%%%%%%%%%%%%%5
    clusteredImage = colorCluster(test);
    
    [nrows ncols] = size(clusteredImage);
    numGroups = max(max(clusteredImage));

    charMask = logical(zeros([nrows, ncols, numGroups]));
    for i = 1:max(max(clusteredImage))
        charMask(:,:,i) = logical(clusteredImage == i);
    end

    % Iterate through masks and clean it up
    blobCellArray = cell(size(charMask,3),1);
    showSteps = false;
    for i = 1:size(charMask,3)
        curBlobs = extractBlobs(charMask(:,:,i), showSteps);
        blobCellArray{i} = cell(size(curBlobs));
        blobCellArray{i} = curBlobs;
    end

    % Iterate through blobs and create histograms for each bounding box
    showCrops = false;
    hsvCellArr = cell(size(charMask,3),1);
    totBlobs = 0;
    for j=1:size(charMask,3)
        blobs = blobCellArray{j};
        mask = charMask(:,:,j);
    %     hsvCellArr{j} = blobToHist(blobs,mask,test,showCrops,meanTrainRow);
        hsvCellArr{j} = blobToHist(blobs,mask,test,showCrops);
        totBlobs = totBlobs + size(hsvCellArr{j},1);
    end

    % Pull from cell array to 2d array
    curIndex = 1;
    hsvMat = zeros(totBlobs, 256);
    origCell = zeros(totBlobs,1);
    blobs = [];
    for i=1:length(hsvCellArr)
        curMat = hsvCellArr{i};
        curBlobs = blobCellArray{i};
        blobs = [blobs; curBlobs];
        hsvMat(curIndex:(curIndex + size(curMat,1) - 1),:) = curMat;
        origCell(curIndex: (curIndex + size(curMat,1)-1)) = i;
        curIndex = curIndex + size(curMat,1);
    end

    %%%%%%%%%%%%%%%% OLD FLOW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
    % % Predict using KNN on trainingData
    % pred = knnclassify(hsvMat,trainingData,labels,3);
    
    %% Predict using Nearest Neighbor
    [pred, dist] = histOverlap(hsvMat, trainingData, labels);
    
    % Drop areas too small
    areaThresh = 100;
    areas = zeros(length(blobs),1);
    for i=1:length(blobs)
        areas(i) = blobs(i).Area;
    end
    blobs = blobs(areas>areaThresh);
    pred = pred(areas>areaThresh);
    dist = dist(areas>areaThresh);
    
    % Drop boundingBoxes too big
    frameArea = nrows*ncols;
    boxThresh = .5;
    boxAreas = zeros(length(blobs),1);
    for i=1:length(blobs)
        boxAreas(i) = blobs(i).BoundingBox(3)*blobs(i).BoundingBox(4);
    end
    blobs = blobs(boxAreas<boxThresh*frameArea);
    pred = pred(boxAreas<boxThresh*frameArea);
    dist = dist(boxAreas<boxThresh*frameArea);
    
    % Drop bounding boxes too skinny
    rectThresh = 4;
    rect = zeros(length(blobs),1);
    for i=1:length(blobs)
        dx = blobs(i).BoundingBox(3);
        dy = blobs(i).BoundingBox(4);
        rect(i) = max(dx/dy,dy/dx);
    end
    blobs = blobs(rect<rectThresh);
    pred = pred(rect<rectThresh);
    dist = dist(rect<rectThresh);
    
    % One match per character
    [blobs, pred, dist] = oneMatchPerChar(blobs, pred, dist);

    % Drop examples too far in distance
    thresh = 1;
    blobs = blobs(dist < thresh);
    pred = pred(dist < thresh);
    dist = dist(dist < thresh);
    % % % % Show Results
%     accuracy = showResults(blobs,mask,test,pred, dist);
    [coords, characters] = locateChars(blobs, pred);
    
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     % EIGEN/FISHER VERSIONS
% 
%     %% EIGEN IMAGES on Color histograms
%     % [pcaQuery, pcaTraining] = computePCA(trainingData,hsvMat);
%     
%     % % Predict using KNN on trainingData
%     % pred = knnclassify(pcaQuery',pcaTraining',labels,3);
%     
%     % % Show Results
%     % accuracy = showResults(blobs,mask,test,pred);
%     
%     %% FISHER IMAGES
%     [fisherQuery, fisherTraining] = createFisher(trainingData, labels, meanTrainRow, hsvMat)
%     
%     % % Predict using KNN on trainingData
%     pred = knnclassify(fisherQuery,fisherTraining,labels,1);
%     
%     % % Show Results
%     % accuracy = showResults(blobs,mask,test,pred);
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end