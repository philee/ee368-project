function [image, frames] = cutThirds(inputImage)
    [nrows, ncols, three] = size(inputImage);
    thirdDist = ncols/3;
    image = inputImage;
    frames = zeros(3,4);
    
    % Y Values are constant
    frames(:,2) = 1;
    frames(:,4) = nrows - 1;
    
    % X Values are thirds
    frames(:,1) = [1; 1 + round(thirdDist); 1 + round(2*thirdDist)];
    frames(:,3) = [round(thirdDist); round(thirdDist); ncols - frames(3,1)];
end