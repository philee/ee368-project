fileList = dir('comics/2010');
for i=3:length(fileList)
    name = fileList(i).name;
    if strcmp('2010',name(1:4))
        [image, map] = imread(strcat('comics/2010/',fileList(i).name));
        if ~isempty(map)
            image = ind2rgb(image, map);
        end
        
        imshow(image);
        [colorImage, frame] = cutThirds(image);
        
        output = overlayNamesColor(colorImage,frame);
        pause
        
    end
end