%% Converts RGB to HSV
% Input: RGB input (with or without color map)
% Output: an HSV image

function newIm = rgbToHSV(image, map)
    try 
       newImage = ind2rgb(image,map);
    catch err
        newImage = image;
    end 
    newIm = rgb2hsv(newImage);
end