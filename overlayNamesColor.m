function [finalImage, inputImage] = overlayNamesColor(inputCamImage, frames)
    addpath(pwd)
    addpath('export_fig');
    iptsetpref('ImshowBorder','tight');
    axis off;
    warning off;
    GARIDX = 1;
    JONIDX = 2;
    ODIEIDX = 3;
    LIZIDX = 4;
    ARLENEIDX = 5; 
    POOKYIDX = 6;
    NERMALIDX = 7;
    MISCIDX = 8;
    
    characters = {'Garfield','Jon','Odie','Liz','Arlene','Pooky','Nermal', 'Misc'};

    %% Train KNN Database if necessary
    if ~exist('TrainingData.mat')
        disp ('Training Database')
        cd comics
        cd train
        numTrainFiles = 0;
        curIndex = 1;
        charIndex = 1;
        for curCharCell = characters
            curChar = curCharCell{1};
            numTrainFiles = numTrainFiles + length(dir(curChar)) - 2;     % minus two is because of '.' and '..'
        end

        trainingData = zeros(numTrainFiles,256);
        labels = zeros(numTrainFiles,1);
        for curCharCell = characters
            curChar = curCharCell{1};
            cd (curChar)
            files = dir('.');
            for i = 3:length(files)
                [databaseIm, dMap] = imread(files(i).name);
                if ~isempty(dMap)
                    databaseIm = ind2rgb(databaseIm,dMap);
                end

                hsvVector = imageToHistVec(databaseIm);
                trainingData(curIndex,:) = hsvVector';
                labels(curIndex) = charIndex;
                curIndex = curIndex + 1;
            end
            charIndex = charIndex + 1;
            cd ..
        end
        cd ..
        cd ..

        meanTrainRow = mean(trainingData);

        for i = 1:size(trainingData,1)
            trainingData(i,:) = trainingData(i,:) / sum(trainingData(i,:));
        end

        save('TrainingData.mat','trainingData','labels','meanTrainRow');
    else
        load TrainingData
    end
    
    %% Resize Image if necessary
    [origRows origCols three] = size(inputCamImage);
%     disp('Resizing');
    inputImage = inputCamImage;
    indicesArray = frames;
    if abs(origCols - 600) > 100
        inputImage = imresize(inputCamImage,[NaN 600]);
        [rows cols] = size(inputImage);

        scale = origCols / 600;
        indicesArray = round(frames/scale);
        indicesArray(:,1:2) = max(1,indicesArray(:,1:2));
        indicesArray(:,3) = min(cols - indicesArray(:,1),indicesArray(:,3));
        indicesArray(:,4) = min(rows - indicesArray(:,2),indicesArray(:,4));
    end
    
    %% Color Balance if necessary
    colorBalanceOn = false;
    if (max(max(max(inputImage))) < 255) & colorBalanceOn
        disp('Color Balancing')
        balancedImage = colorBalanceCorners(inputImage);
    end

    %% QUERY
    finalCoords = [];
    charList = {};
    % For each frame
    curCell = 0;
    for i=1:size(indicesArray,1)
        x = indicesArray(i,1);
        y = indicesArray(i,2);
        dx = indicesArray(i,3);
        dy = indicesArray(i,4);
        curCrop = inputImage(y:(y+dy),x:(x+dx),:);
%         figure();
%         imshow(curCrop);
        [curCoord, curChar] = analyzeFrame(curCrop, trainingData, labels);
        globalCoord = (curCoord) + repmat([x y],size(curCoord,1),1);
        
        finalCoords = [finalCoords; globalCoord];
        for i=1:length(curChar)
            charList{i+curCell} = curChar{i};
        end
        curCell = curCell + length(curChar);
    end
    
    % Overlay Text
    figure('units','normalized','outerposition',[0 0 1 1]);
    image(inputImage);
    
    for i=1:length(charList)
        charName = charList{i};
        text(double(finalCoords(i,1)), double(finalCoords(i,2)), charName, 'FontSize',18,'HorizontalAlignment','center','EdgeColor','k','LineWidth',1,'BackgroundColor','w');
    end
    
    iptsetpref('ImshowBorder','tight');
    axis off;
    
%     print(gcf,'-dbmp','tempColorImg.bmp');
    export_fig tempColorImg.bmp -nocrop;
    [finalImage, map] = imread('tempColorImg.bmp');
    if ~isempty(map)
        finalImage = ind2rgb(finalImage,map);
    end
    figure(); imshow(finalImage);
%     figure(); imshow(balancedImage);
    delete('tempColorImg.bmp');
end