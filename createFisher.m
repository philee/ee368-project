function [fisherQuery, fisherTraining] = createFisher(trainingData, labels, meanTrainRow, hsvMat)
% % % SEPARATE CLASSES

    trainingCells = cell(8,1);
    for i=1:8
        indices = (labels == i);
        trainingCells{i} = trainingData(indices,:);
    end

    % CREATE R_between
    Rb = zeros(size(meanTrainRow'*meanTrainRow));
    for i=1:8
        trainingClass = trainingCells{i};
        numClass = size(trainingClass,1);
        if numClass > 0
            classMeanRow = mean(trainingClass);
            classMeanVec = classMeanRow';
            totMeanVec = meanTrainRow';
            Rb = Rb + numClass*(classMeanVec - totMeanVec)*(classMeanVec' - totMeanVec');
        end
    end

    % Create R_within
    Rw = zeros(size(meanTrainRow'*meanTrainRow));
    for i=1:8
        trainingClass = trainingCells{i};
        classMeanRow = mean(trainingClass);
        classMeanVec = classMeanRow';
        totMeanVec = meanTrainRow';
        numClass = size(trainingClass,1);
        for j=1:numClass
            curVec = trainingClass(j,:)';
            Rw = Rw + (curVec - classMeanVec)*(curVec' - classMeanVec');
        end
    end

    % Fisher Vectors
    [V,D] = eigs(Rb, Rw);
    fisherVectors = V;

    fisherTraining = trainingData * fisherVectors;
    fisherQuery = hsvMat * fisherVectors;
end