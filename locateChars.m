function [coords, predChars] = locateChars(blobs, pred)
    characters = {'Garfield','Jon','Odie','Liz','Arlene','Pooky','Nermal', 'Misc'};   
    predChars = cell(length(blobs),1);
    coords = zeros(length(blobs),2);
    
    for i=1:length(blobs)
        curBBox = blobs(i).BoundingBox;
        x = curBBox(1);
        y = curBBox(2);
        dx = curBBox(3);
        dy = curBBox(4);
        
        centerX = round((2*x+dx)/2);
        centerY = round((2*y+dy)/2);
        coords(i,:) = [centerX, centerY];
        predChars{i} = characters{pred(i)};
    end
    
end