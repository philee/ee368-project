function blobs = extractBlobs(bwImage, showSteps)
    
    % Clean up image
    small = strel('disk',3);
    medium = strel('disk',5);
    big = strel('disk',8);
    huge = strel('disk',12);
    side = strel('line',3,0);
    vert = strel('line',3,90);
    
%     maskCleanVert = bwImage;
    maskCleanMed = medfilt2(bwImage,[2 2]);
    maskCleanSide = imopen(maskCleanMed,side);
    maskCleanSide = imclose(maskCleanSide,side);
    maskCleanVert = imopen(maskCleanSide,vert);
    maskCleanVert = imclose(maskCleanVert,vert);
    maskCleanClose = imclose(maskCleanVert,small);
    maskClean = imopen(maskCleanClose,small);
    
    if showSteps
        figure(); imshow(bwImage);
%         figure(); imshow(maskCleanSide);
%         figure(); imshow(maskCleanVert);
%         figure(); imshow(maskCleanClose);
        figure(); imshow(maskClean);
    end
    
    blobs = regionprops(maskClean,'BoundingBox','Area');
end