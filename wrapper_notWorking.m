% Overall Wrapper
close all;
% clear all;

addpath(pwd)

GARIDX = 1;
JONIDX = 2;
ODIEIDX = 3;
LIZIDX = 4;
ARLENEIDX = 5; 
POOKYIDX = 6;
NERMALIDX = 7;
MISCIDX = 8;
characters = {'Garfield','Jon','Odie','Liz','Arlene','Pooky','Nermal', 'Misc'};

%% Train KNN Database
if ~exist('TrainingData.mat') | true;
    cd comics
    cd train
    numTrainFiles = 0;
    curIndex = 1;
    charIndex = 1;
    for curCharCell = characters
        curChar = curCharCell{1};
        numTrainFiles = numTrainFiles + length(dir(curChar)) - 2;     % minus two is because of '.' and '..'
    end

    trainingData = zeros(numTrainFiles,256);
    labels = zeros(numTrainFiles,1);
    for curCharCell = characters
        curChar = curCharCell{1};
        cd (curChar)
        files = dir('.');
        for i = 3:length(files)
            [databaseIm, dMap] = imread(files(i).name);
            if ~isempty(dMap)
                databaseIm = ind2rgb(databaseIm,dMap);
            end
            hsvVector = imageToHistVec(databaseIm);
            trainingData(i,:) = hsvVector';
            labels(curIndex) = charIndex;
            curIndex = curIndex + 1;
        end
        charIndex = charIndex + 1;
        cd ..
    end
    cd ..
    cd ..
    
    for i=1:length(trainingData)
        trainingData(i,:) = trainingData(i,:) / norm(trainingData(i,:));
    end
    
    meanTrainRow = mean(trainingData);
    trainingData = trainingData - repmat(meanTrainRow,size(trainingData,1),1);
    

    
    save('TrainingData.mat','trainingData','labels','meanTrainRow');
else
    load TrainingData
end

%% QUERY

% Read in a test image
% [test1, map] = imread('1991_ga910127.png');
[test1, map] = imread('2014_ga140126.png');
% [test1, map] = imread('1991_ga910129.png');
% [test1, map] = imread('2012_ga120506.png');


% Convert to correct format
if ~isempty(map)
    test = ind2rgb(test1,map);
else
    test = test1;
end

% 3D array with a binary image for each character
charMask = logical(createCharMasks(test));

% Iterate through masks and clean it up
blobCellArray = cell(size(charMask,3),1);
showSteps = false;
for i = 1:size(charMask,3)
    curBlobs = extractBlobs(charMask(:,:,i), showSteps);
    blobCellArray{i} = cell(size(curBlobs));
    blobCellArray{i} = curBlobs;
end

% Iterate through blobs and create histograms for each bounding box
showCrops = false;
hsvCellArr = cell(size(charMask,3),1);
totBlobs = 0;
for j=1:size(charMask,3)
    blobs = blobCellArray{j};
    mask = charMask(:,:,j);
    hsvCellArr{j} = blobToHist(blobs,mask,test,showCrops);
    totBlobs = totBlobs + size(hsvCellArr{j},1);
    hsvCellArr{j} = hsvCellArr{j} - repmat(meanTrainRow,size(hsvCellArr{j},1),1);
    curMat = hsvCellArr{j};
%     for k=1:size(curMat,1)
%         curMat(k,:) = curMat(k,:)/norm(curMat(k,:));
%     end
    hsvCellArr{j} = curMat;
end

% Pull from cell array to 2d array
curIndex = 1;
hsvMat = zeros(totBlobs, 256);
origCell = zeros(totBlobs,1);
for i=1:length(hsvCellArr)
    curMat = hsvCellArr{i};
    hsvMat(curIndex:(curIndex + size(curMat,1) - 1),:) = curMat;
    origCell(curIndex: (curIndex + size(curMat,1)-1)) = i;
    curIndex = curIndex + size(curMat,1);
end

%% FISHER IMAGES
% [fisherQuery, fisherTraining] = createFisher(trainingData,labels, meanTrainRow, hsvMat)
% % Predict using KNN on trainingData
% predictions = knnclassify(fisherQuery,fisherTraining,labels,3);

% % Predict and check for matching items
predictions = knnclassify(hsvMat,trainingData,labels,3);
[nearestNeighbor, distances] = knnsearch(trainingData, hsvMat);
% Kill Matches that don't fit color
eliminateList = reduceCandidates(hsvMat,origCell,predictions);

% Show results
curIndex = 0;
for i=1:length(blobCellArray)
    blobs = blobCellArray{i};
    mask = charMask(:,:,i);
    pred = predictions((curIndex + 1):(length(blobs) + curIndex));
    keep = ~eliminateList((curIndex + 1):(length(blobs) + curIndex));
    dist = distances((curIndex + 1):(length(blobs) + curIndex));
    curIndex = curIndex + length(blobs);
    blobs = blobs(keep);
    pred = pred(keep);
    dist = dist(keep);
    accuracy = showResults(blobs,mask,test,pred, dist);
end

%%%%%%%%%%%%%%%% OLD FLOW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mask out good colors
% mask = maskImage(test);

% Extract binary blobs
% showSteps = true;
% blobs = extractBlobs(mask, showSteps);
% 
% % % % Create Histograms from each bounding box
% % showCrops = false;
% % hsvMat = blobToHist(blobs,mask,test,showCrops);
% % hsvMat = hsvMat - repmat(meanTrainRow,size(hsvMat,1),1);
% 
% % % % Predict using KNN on trainingData
% % pred = knnclassify(hsvMat,trainingData,labels,3);
% 
% % % % Show Results
% % accuracy = showResults(blobs,mask,test,pred);
% 
% %% EIGEN IMAGES on Color histograms
% % [pcaQuery, pcaTraining] = computePCA(trainingData,hsvMat);
% 
% % % Predict using KNN on trainingData
% % pred = knnclassify(pcaQuery',pcaTraining',labels,3);
% 
% % % Show Results
% % accuracy = showResults(blobs,mask,test,pred);
% 
% %% FISHER IMAGES
% [fisherQuery, fisherTraining] = createFisher(trainingData, labels, meanTrainRow, hsvMat)
% 
% % % Predict using KNN on trainingData
% pred = knnclassify(fisherQuery,fisherTraining,labels,1);
% 
% % % Show Results
% % accuracy = showResults(blobs,mask,test,pred);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%% AUTOMATIC COLOR SEGMENTATION%%%%%%%%%%%%%%%%%%%%%%%%%5
% clusteredImage = colorCluster(test);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

