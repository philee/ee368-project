function charMasks = createCharMasks(rgbImage)
    colorImage = rgbImage;
    GARIDX = 1;
    JONIDX = 2;
    ODIEIDX = 3;
    LIZIDX = 4;
    ARLENEIDX = 5; 
    POOKYIDX = 6;
    NERMALIDX = 7;
    MISCIDX = 8;
    characters = {'Garfield','Jon','Odie','Liz','Arlene','Pooky','Nermal', 'Misc'};

    orangeMask = colorMask(colorImage,29/360,37/360,.6,1,.7,1);
    garfieldOldMask = colorMask(colorImage,35/360,40/360, .85,1,.95,1);
    yellowMask = colorMask(colorImage,50/360,60/360,.35,.55,.9,1);
    tanMask = colorMask(colorImage, 25/360,50/360,.2,.4,.9,1);
    grayMask = colorMask(colorImage, 40/360, 100/360, 0, .20,.25,.75);
    brownMask = colorMask(colorImage,15/360,25/360, .65, .9, .4, .7);
    blackMask = colorMask(colorImage,0,1,0,1,0,.1);
    stripeMask = colorMask(colorImage,25/360,35/360, .9,1,.2,.4);
    oldStripeMask = colorMask(colorImage,35/360, 40/36, 80, 100, 30, 60); 
%     whiteMask = colorMask(colorImage,0,1,0,.1,.95,1);
%     redMask = colorMask(colorImage,330/360,1,0,1,0,1) | colorMask(colorImage,0,30/360,0,1,0,1);

    maskArray = zeros([size(rgbImage,1) size(rgbImage,2) length(characters)]);
    maskArray(:,:,GARIDX) = orangeMask | garfieldOldMask | stripeMask | oldStripeMask;
    maskArray(:,:,JONIDX) = tanMask ;
    maskArray(:,:,ODIEIDX) = yellowMask | brownMask;
    maskArray(:,:,LIZIDX) = tanMask | blackMask;
    maskArray(:,:,ARLENEIDX) = zeros([size(colorImage,1) size(colorImage,2)]);
    %% SEE IF THIS NEEDS CHANGING
    maskArray(:,:,POOKYIDX) = brownMask;
    maskArray(:,:,NERMALIDX) = grayMask;
    % WHAT TO DO?
    maskArray(:,:,MISCIDX) = grayMask;
    charMasks = maskArray;
end

    
% Scale is now [0,1] for HSV not HSL. VERY DIFFERENT.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% HSV COLORS
% Jon - 39/360, .31, .98; 30/360, 27,97
% Garfield - 34/360, .79, .93; 35/360, .85, .91
% Odie - 56/360, 44,100; BAD - 60, .34 .98
% Jon's hair - 15/360, .66, .6; BAD: 43/360, .34, .71
% Nermal - 60/360, .07, .64
% Garfield Stripes - 30, 100, 35; 37, 89, 44

% FORMAT IS: color image, min hue, max hue, min sat, max sat, min val, max
% val