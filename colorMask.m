% Creates a bit mask for values in [minHue, maxHue] range
% Doesn't do anything about saturation or value
% Input: RGB Image, min hue, max hue
% Output: RGB Mask

function mask = colorMask(image, minHue, maxHue, minSat, maxSat, minVal, maxVal)
    
    hsv = rgbToHSV(image);
    hue = hsv(:,:,1);
    sat = hsv(:,:,2);
    val = hsv(:,:,3);
    
%     if minHue > 1
%         minHueVal = minHue / 360;
%         maxHueVal = maxHue / 360;
%         minSatVal = minSat / 100;
%         maxSatVal = maxSat / 100;
%         minValueVal = minVal / 100;
%         maxValueVal = maxVal / 100;
%     end

    minHueVal = minHue;
    maxHueVal = maxHue;
    minSatVal = minSat;
    maxSatVal = maxSat;
    minValueVal = minVal;
    maxValueVal = maxVal;
    
    % Hue
    hueMaskMin = zeros(size(hue));
    hueMaskMax = zeros(size(hue));
    hueMaskMin(hue >= minHueVal) = 1;
    hueMaskMax(hue <= maxHueVal) = 1;
    
    % Sat
    satMaskMin = zeros(size(hue));
    satMaskMax = zeros(size(hue));
    satMaskMin(sat >= minSatVal) = 1;
    satMaskMax(sat <= maxSatVal) = 1;
    
    % Val
    valMaskMin = zeros(size(hue));
    valMaskMax = zeros(size(hue));
    valMaskMin(val >= minValueVal) = 1;
    valMaskMax(val <= maxValueVal) = 1;
    
    mask = hueMaskMin & hueMaskMax & valMaskMin & valMaskMax & satMaskMin & satMaskMax;
end