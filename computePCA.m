function [pcaQuery, pcaTraining] = computePCA(trainingData, hsvMat)
    PCA_TO_KEEP = 128;

    S = trainingData;
    [u s v] = svd(S.'*S);
    pcaVec = S*v;
    pcaVec = pcaVec(1:PCA_TO_KEEP,:);

    for i=1:size(pcaVec,2)
        pcaVec(:,i) = pcaVec(:,i)/norm(pcaVec(:,i));
    end

    pcaTraining =  pcaVec * trainingData';
    pcaQuery = pcaVec * hsvMat';
end