function [predictions, distances] = histOverlap(queries, trainingData, labels)
    
    predictions = zeros(size(queries,1),1);
    distances = zeros(size(predictions));
    for i=1:size(queries,1)
        curVec = queries(i,:);
        curVecRep = repmat(curVec,size(trainingData,1),1);
        
        useAbsDiff = true;
        useSim = ~useAbsDiff;
        % Using Abs Differences
        if (useAbsDiff)
            diff = abs(curVecRep - trainingData);
            curDist = sum(diff,2);
        end
        
        % Using Similarity
        if useSim
            similarity = min(curVecRep, trainingData);
            curDist = -sum(similarity,2);  
        end
        
        bestMatchIndex = find(curDist == min(curDist));
        predictions(i) = labels(bestMatchIndex);
        distances(i) = min(curDist);
    end
end