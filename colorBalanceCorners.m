function balancedImage = colorBalanceCorners(colorImg, showImages)
    if nargin < 2
        showImages = false;
    end
    
    histStretch = double(colorImg);
        
    % Histogram Stretching
    histStretch(:,:,1) = histStretch(:,:,1) - min(min(histStretch(:,:,1)));
    histStretch(:,:,1) = (histStretch(:,:,1) / max(max(histStretch(:,:,1))));
    histStretch(:,:,2) = histStretch(:,:,2) - min(min(histStretch(:,:,2)));
    histStretch(:,:,2) = (histStretch(:,:,2) / max(max(histStretch(:,:,2))));
    histStretch(:,:,3) = histStretch(:,:,3) - min(min(histStretch(:,:,3)));
    histStretch(:,:,3) = (histStretch(:,:,3) / max(max(histStretch(:,:,3))));
        
    % Find 4 corners. Assume 1 is white
    corner1 = reshape(histStretch(1,1,:),3,1);
    corner2 = reshape(histStretch(1,end,:),3,1);
    corner3 = reshape(histStretch(end,1,:),3,1);
    corner4 = reshape(histStretch(end,end,:),3,1);
    
    corners = [corner1, corner2, corner3, corner4];
    
    diff = abs(corners(1,:)*-2 + corners(2,:) + corners(3,:));
    
    minDiffIndex = find(diff == min(diff));
    minDiffIndex = minDiffIndex(1);
    
    whiteGuess = corners(:,minDiffIndex);
    
    gRatio = whiteGuess(2) / whiteGuess(1);
    bRatio = whiteGuess(3) / whiteGuess(1);
    
    balanced = histStretch;
    
%     balanced(:,:,2) = balanced(:,:,2) / gRatio;
%     balanced(:,:,3) = balanced(:,:,3) / bRatio;
%     
%     balanced = balanced / max(max(max(balanced)));
%     balancedImage = uint8(round(balanced * 255));
    balancedImage = balanced;
    if showImages
        figure(); imshow(colorImg);
        figure(); imshow(histStretch);
        figure();imshow(balancedImage);
    end
end