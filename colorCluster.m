function clusteredImage = colorCluster(test)
%     kRange = 3:10;
%     figure(); imshow(test); 

    numClusters = 6;
    cform = makecform('srgb2lab');
    labImage = applycform(test,cform);
    labIm = double(labImage(:,:,2:3));
    nrows = size(labIm,1);
    ncols = size(labIm,2);
    labVectors = reshape(labIm,nrows*ncols,2);

    % Seed random number generator
    rand('seed',0);

        % Kmeans
    [cluster_idx, cluster_center] = kmeans(labVectors, numClusters,'Replicate',3,'EmptyAction','drop');
    %[cluster_idx, centers] = kmeans(labVectors, numClusters);
    
    pixel_labels = reshape(cluster_idx, nrows, ncols);
%     disp(numClusters);
%     figure(); imshow(pixel_labels,[]);

    clusteredImage = pixel_labels;
        
end