function predChars = showResults(blobs,mask,colorImage,pred, distances)
    characters = {'Garfield','Jon','Odie','Liz','Arlene','Pooky','Nermal', 'Misc'};
    
    vertOffset = 10;
    horzOffset = 5;    
    [rows cols] = size(mask);
    predChars = cell(length(blobs),1);
    
    for i=1:length(blobs)
        curBBox = blobs(i).BoundingBox;
        x = max(floor(curBBox(1)-horzOffset),1);
        y = max(floor(curBBox(2)-vertOffset),1);
        maxX = min(cols,x+curBBox(3) + 2*horzOffset);
        maxY = min(rows,y+curBBox(4) + vertOffset);
        curImage = colorImage(y:maxY,x:maxX,:);
        if nargin <= 4
            figure(); imshow(curImage); title(strcat({'Predicted: '},characters{pred(i)}));
        else
            figure(); imshow(curImage); title(strcat({'Predicted: '},characters{pred(i)},{' Distance: '},{num2str(distances(i))}));
        end
       predChars{i} = characters{pred(i)};
    end
end