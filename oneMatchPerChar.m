function [oneBlob, onePred, oneDist] = oneMatchPerChar(blob, pred, dist)
    keep = logical(zeros(size(pred)));
    for i = 1:max(pred)
        if sum(pred == i) > 0
            tempDist = dist;
            tempDist(pred ~= i) = abs(3*max(dist));
            closest = find(tempDist == min(tempDist));
            closest = closest(1);
            keep(closest) = 1;
        end
    end
    
    oneBlob = blob(keep);
    onePred = pred(keep);
    oneDist = dist(keep);
    
end