function isometry = estIsometry(X1,X2)
	numExamples = length(X2);
	variables = zeros(6,1);
	
	cart2 = X2(1:2,:);
	Y = cart2(:);
	A = zeros(2*numExamples,6);
	
	for i=1:numExamples
		A(2*i-1,:) = [X1(:,i)' zeros(1,3)];
		A(2*i,:) = [zeros(1,3) X1(:,i)'];
	end
	
	Xls = (A'*A)^-1 * A'* Y;
	temp = reshape(Xls,3,2)';
	isometry = [temp; 0 0 1];
end