function hsvHistVec = imageToHistVec(rgbImage)
    curHSV = rgbToHSV(rgbImage);
    hsvHistVec = imhist(curHSV(:,:,1));
end