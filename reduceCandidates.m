function badCandidates = reduceCandidates(hsvMat,origCell,pred)
%     colorImage = rgbImage;
%     GARIDX = 1;
%     JONIDX = 2;
%     ODIEIDX = 3;
%     LIZIDX = 4;
%     ARLENEIDX = 5; 
%     POOKYIDX = 6;
%     NERMALIDX = 7;
%     MISCIDX = 8;
%     characters = {'Garfield','Jon','Odie','Liz','Arlene','Pooky','Nermal', 'Misc'};
    
    badCandidates = origCell ~= pred;

end
